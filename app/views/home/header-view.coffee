View = require 'views/base/view'

module.exports = class HeaderView extends View
  events:
    "typeahead.selected": "doStuff"

  autoRender: false
  className: 'header'
  tagName: 'input'
  attributes:
    "type": "search"

  initalize: ->
    this.twitterSource = new Bloodhound
        limit: 5
        datumTokenizer: (d) ->
          # Bloodhound.tokenizers.whitespace d.result

        queryTokenizer: Bloodhound.tokenizers.whitespace

        dupDetector: (a, b) ->
          a.url is b.url

        remote:
          url: "https://twitter.com/search?q=%QUERY"

        filter: (response) ->
          response.hits

  render: ->
    # FIXME: this is NOT the Backbone way to do this...
    $('[data-target~=tt-placholder]').append this.el

    this.$el.typeahead(
      highlight: true

      name: "twitter"
      displayKey: "title"
      source: this.twitterSource.ttAdapter()
      templates:
        suggestion: templates.suggestion
        header: templates.header
        footer: templates.footer
        empty: templates.empty
    )

  doStuff: ->

